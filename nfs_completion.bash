if [[ -n ${ZSH_VERSION-} ]]; then
	autoload -U +X bashcompinit && bashcompinit
fi

_nfs_complete_command()
{
	for f in $NFS_LIB/nfs-*; do
		echo $f | awk -F'-' '{print $NF}'
	done
}

_nfs_list_machines()
{
	ls $($exe -E NFS_PROFILE_DIR)
}

_nfs_list_servers()
{
	for p in $($exe -E NFS_PROFILE_DIR)/*; do
		unset IS_SERVER
		. $p
		if [ "$IS_SERVER" == "true" ]; then
			echo $(basename $p)
		fi
	done
}

_nfs_extract_custom()
{
	echo "$meta" | grep -m1 "^#% $1" | awk -F"#% $1" '{for (i=2; i<= NF; i++) print $i}'
}

_nfs_list_args()
{
	case $1 in
	CUSTOM) _nfs_extract_custom "CUSTOM-$prev" ;;
	CUSTOMCMD) eval $(_nfs_extract_custom "CUSTOMCMD-$prev") ;;
	COMMANDS) _nfs_complete_command  ;;
	ENV) $exe -e | awk -F'=' '{print $1}' ;;
	MACHINES) _nfs_list_machines ;;
	NFS) echo "2 3 4 4.1 4.2" ;;
	RUNME) 
		slice=${COMP_WORDS[@]:0:$COMP_CWORD-1}
		$slice $(_nfs_extract_custom "RUNME-$prev") $cur
		;;
	SERVERS) _nfs_list_servers ;;
	TODO) echo "TODO" ;;
	esac
}

_nfs_complete_arg()
{
	if [ "$1" == "FILE" ]; then
		COMPREPLY=($(compgen -f $cur))
	elif [ "$1" == "DIRECTORY" ]; then
		COMPREPLY=($(compgen -d $cur))
	else
		COMPREPLY=($(compgen -W "$(_nfs_list_args $1)" -- "${cur}"))
	fi
}

_nfs_complete_script()
{
	# Complete options list
	opts=$(echo "$meta" | grep "^#% -" | awk '{print $2}')

	# Add in trailing arguments
	arg=$(echo "$meta" | grep -m1 "^#% ARGS" | awk '{print $3}')
	extra=$(_nfs_list_args $arg)

	COMPREPLY=($(compgen -W "${opts} ${extra}" -- "${cur}"))
}

_nfs_do_completion()
{
	meta=$(grep "^#%" $1)

	arg=$(echo "$meta" | grep -m1 "^#% -$prev" | awk -F'[<>]' '{print $3}')
	if [ ! -z "$arg" ]; then
		_nfs_complete_arg $arg
	else
		_nfs_complete_script $1
	fi
}

_nfs()
{
	COMPREPLY=()

	eval exe=${COMP_WORDS[0]}
	exe=$(which $exe)

	if [ "$exe" == "/usr/bin/nfs" ]; then
		NFS_LIB=/usr/share/nfs-ordeal/
	else
		NFS_LIB=$(dirname $exe)
	fi

	cmd=${COMP_WORDS[1]}
	cur=${COMP_WORDS[COMP_CWORD]}
	prev=${COMP_WORDS[COMP_CWORD - 1]}
	if [ "$prev" != "$cmd" ]; then
		prev=${prev:${#prev} - 1}
	fi

	if [ -f "$NFS_LIB/nfs-$cmd" ]; then
		_nfs_do_completion "$NFS_LIB/nfs-$cmd"
	else
		_nfs_do_completion "$exe"
	fi
}

complete -o filenames -F _nfs nfs


DIRS=$(shell find share/ -links 2 -type d | sed 's/ /\\ /g')
INSTALL=install -v -C
VERSION=$(shell git describe --tags --abbrev=0)

$(DIRS):
	$(INSTALL) -d /usr/"$@"
	$(INSTALL) "$@"/* -t /usr/"$@"/

install: $(DIRS)
	$(INSTALL) bin/nfs /usr/bin/
	$(INSTALL) -m 644 README /usr/share/nfs-ordeal/

uninstall:
	rm -v -r /usr/bin/nfs /usr/share/nfs-ordeal

release:
	git archive -o nfs-ordeal-$(VERSION).tar.gz --prefix=nfs-ordeal/ HEAD

.PHONY: install $(DIRS)

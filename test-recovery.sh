#!/bin/bash
# Copyright (c) 2013 Netapp, Inc.  All rights reserved
#% OPTIONS forget_clients forget_locks forget_openowners forget_delegations recall_delegations

# Wait for grace period to end (true/false)?
GRACE_WAIT=true

# Number of test iterations to take?
ITERATIONS=1

# Disable printing and clearing dmesg?
DISABLE_DMESG=true

function generic_recovery
{
	nfs ssh -o "-t -t" $1 "./recover.py $TEST_DIR $2" &
	PID=$!

	for i in $(seq 5); do
		echo ""
		sleep 3
		nfs ssh $1 "sync; sudo su -c 'echo 3 > /proc/sys/vm/drop_caches'"
		if [ "$OPTION" == "reboot_server" ]; then
			nfs halt -r $SERVER
		else
			nfs nfsfault -s $SERVER -f $OPTION -c $1
			nfs nfsfault -s $SERVER -f $OPTION -c $1 -p
		fi

		# Trigger recovery
		nfs ssh $1 "killall -s CONT python"

		# Wait for all recovery to finish before going on
		if [ "$OPTION" == "reboot_server" ]; then
			echo "Waiting for server's grace period to end..."
			tmp=$(mktemp -u --tmpdir=$TEST_DIR)
			nfs ssh $clp "echo tmp > $tmp; rm $tmp"
		fi

		nfs_print_dmesg
		echo ""
	done

	wait $PID
	nfs ssh $1 "sync; sudo su -c 'echo 3 > /proc/sys/vm/drop_caches'"
}

function test_write_recovery
{
	echo ""
	echo "#######################"
	echo "#                     #"
	echo "# Test Write Recovery #"
	echo "#                     #"
	echo "#######################"
	generic_recovery $1 write
}

function test_read_recovery
{
	echo ""
	echo "######################"
	echo "#                    #"
	echo "# Test Read Recovery #"
	echo "#                    #"
	echo "######################"
	generic_recovery $1 read
}

function run_test
{
	case $OPTION in
	forget_clients|forget_locks|forget_openowners|\
	forget_delegations|recall_delegations|reboot_server)
			;;
	*)
		echo -n "Missing or incorrect test option: nfs run -t recovery@"
		echo -n "{forget_clients,forget_locks,forget_openowners,"
		echo -n "forget_delegations,recall_delegations}"
	esac

	for client in $CLIENTS; do
		clp=$(get_client $client)
		echo "client: $clp"
		echo "test: $OPTION"

		test_write_recovery $clp
		echo ""
		test_read_recovery $clp
	done
}

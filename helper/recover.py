#!/usr/bin/env python
# Copyright (c) 2013 Netapp, Inc.  All rights reserved
#
# Use to test state recovery
# Usage: python recover.py TEST_DIR {read, write}

import os
import sys
import fcntl
import signal

def sig_continue(signum, stack_frame):
	pass
signal.signal(signal.SIGCONT, sig_continue)

opts="wb"
if sys.argv[2] == "read":
	opts="rb"
f1 = open(os.path.join(sys.argv[1], "state_recover_1"), opts, 0)
f2 = open(os.path.join(sys.argv[1], "state_recover_2"), opts, 0)

def read_lines(f):
	for i in range(5):
		print(f.readline().decode().strip())
	print()

def write_lines(f, begin):
	end  = begin + 4
	name = f.name.rpartition(os.sep)[2]
	num  = name[len(name) - 1]
	print("Writing to file: %s (line numbers: %s -> %s)" % (name, begin, end))
	for i in range(begin, end + 1):
		text = "State recovery file: %s line: %s\n" % (num, i)
		f.write(text.encode())

def do_io():
	if sys.argv[2] == "read":
		read_lines(f1)
		read_lines(f2)
	else:
		write_lines(f1, do_io.status)
		write_lines(f2, do_io.status)
		print()
		do_io.status += 5
	signal.pause()
do_io.status = 1

# Initial IO
do_io()

# Recover
do_io()

# Attempt locking to force recovery
print("Locking file: " + f1.name)
if sys.argv[2] == "read":
	fcntl.flock(f1, fcntl.LOCK_SH | fcntl.LOCK_NB)
else:
	fcntl.flock(f1, fcntl.LOCK_EX | fcntl.LOCK_NB)
do_io()

# IO through locked state
print("IO through lock stateid for file: " + f1.name)
do_io()

# Attempt unlocking to force recovery
print("Unlocking file: " + f1.name)
fcntl.flock(f1, fcntl.LOCK_UN)
do_io()

# Attempt to close file to force recovery
print("Closing file: " + f1.name)
f1.close()
print("Closing file: " + f2.name)
f2.close()

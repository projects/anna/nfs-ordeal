#!/bin/sh
# Copyright (c) 2013 Netapp, Inc.  All rights reserved.


##
#
# Basic functions for sub-commands
#

function check_profile_exists
{
	[ "$(is_virtual $1)" == "true" ] && return 0
	[ -f "$NFS_CONFIG_DIR/profiles/$1" ] && return 0
	[ -f ".nfs-ordeal/profiles/$1" ] && return 0
	echo "No such profile: $1"
	exit 1
}

function load_profile
{
	check_profile_exists $1
	[ -f "$NFS_CONFIG_DIR/profiles/$1" ] && . $NFS_CONFIG_DIR/profiles/$1
	[ -f ".nfs-ordeal/profiles/$1" ] && . .nfs-ordeal/profiles/$1 || true
}

function load_test
{
	[ -f "$NFS_SHARE/tests/$1" ] && . $NFS_SHARE/tests/$1
	[ -f "$NFS_CONFIG_DIR/tests/$1" ] && . $NFS_CONFIG_DIR/tests/$1
	[ -f ".nfs-ordeal/tests/$1" ] && . .nfs-ordeal/tests/$1
	return 0
}

function retry_cmd
{
	cmd=$1
	shift 1
	for i in $(seq 60); do # Wait for 1 minute
		$cmd $*
		[ $? == 0 ] && return 0
		sleep 1
	done
	return 1
}

function foreach_parallel
{
	pids=
	cmd=$1
	shift 1

	for arg in $*; do
		$cmd $arg &
		pids="$pids $!"
	done

	ret=0
	for pid in $pids; do
		wait $pid
		[ $? != 0 ] && let ret+=1
	done

	[ $ret != 0 ] && exit $ret
	return 0
}

##
# $1: Log identifier
# $2: Function
# $3: Log message
# $4: Exit on error? (true / false)
# $*: Configuration list.  Each entry will be split on comma before calling $2
function foreach_log
{
	dir=.nfs-ordeal/output/$1
	log=.nfs-ordeal/output/$1.txt
	cmd="$2"
	message="$3"
	exit_error=$4
	shift 4

	mkdir -p $dir
	[ -f $log ] && rm $log
	touch $log

	success=0
	failed=0

	set +e
	for c in $*; do
		cfg=$(echo $c | sed 's/,/ /g')
		$cmd $cfg | tee $dir/$c.txt

		if [ ${PIPESTATUS[0]} == 0 ]; then
			let success=$success+1
			echo "$message$cfg: SUCCESS!" >> $log
		else
			let failed=$failed+1
			echo "$message$cfg: FAILED =(" >> $log
			echo "" >> $log

			cat $dir/$c.txt | awk '{
				if ($0 != "")
					print "	",$0;
				else
					print $0;
			}' | cat -s | tail -n 10 >> $log

			echo >> $log

			[ $exit_error == true ] && break
		fi
	done

	echo >> $log
	echo "Test results: $success succeeded, $failed failed" >> $log

	echo -e "\n"
	cat -s $log
}

function nfs_addr
{
	load_profile $1
	if [ ! -z "$ADDRESS" ]; then
		echo $ADDRESS
		return 0
	else
		mac=$(nfs_virsh dumpxml $1 | grep "mac address=" | awk -F"'" '{print $2}')
		ip neighbor | grep "$mac" | awk '{print $1}'
	fi
}



###
#
# libvirt related functions
#

function is_virtual
{
	[ -z $(nfs_virsh "list --all --name" | grep "^$1$") ] && echo "false" || echo "true"
}

function nfs_virsh
{
	virsh -q -c $NFS_LIBVIRT_CONNECTION $* 2>&1
}

function is_active
{
	[ "$(nfs_virsh list | grep "$1")" ] && echo "true" || echo "false"
}

function is_inactive
{
	[ "$(nfs_virsh list --inactive | grep "$1")" ] && echo "true" || echo "false"
}



###
#
# Other machine-related functions
#

function is_running
{
	if [ "$(is_virtual $1)" ]; then
		is_active $1
		return
	fi

	ping -c 1 -W 1 $(nfs_ip $1) > /dev/null
	[ $? == 0 ] && echo "true" || echo "false"
}
